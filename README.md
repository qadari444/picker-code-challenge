Pickar code Challenge 

### Used technoloiges  

- HTML
- CSS
- Bootstrap
- React

### Requirements

- NodeJS  V10.17.0 or greater 
- Npm  V6.11.3 or greater 

## Available Scripts To Run the Project

first clone the prject with the follwing command:

`git clone https://gitlab.com/qadari444/picker-code-challenge.git`

once the project cloned, In the project directory, you can run:

### 1. npm install 
### 2. npm start

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
