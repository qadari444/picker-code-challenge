import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.min.css';
import './App.css'; 
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory'; 
import Converter from './views/Converter'
import History from './views/History'

class App extends Component {

  render() {
    const history = createHistory();
    return (
      <Router history={history}>
        <Switch>
          <Route exact path="/" render={() => (<Redirect to="/converter" />)} />
          <Route exact path="/converter" component={Converter}/>
          <Route exact path="/converter/history" component={History}/>
        </Switch>
      </Router>

        );
  }
}

export default App;
