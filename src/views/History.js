import React from 'react';
import { Link } from 'react-router-dom'

import TableRow from '../components /TabeRow'


function History() {
    return (
        <div class="converter">
            <div class="history">
                <div class="container-fluid">
                    <div class="brand text-white">pickar</div>
                    <Link class="history-link text-white pl-5 d-block mt-5 mb-3" style={{ fontSize: '13px' }} to='/converter'>
                        <i className="fa fa-angle-left">
                        </i> Go Back</Link>

                    <ul class="responsive-table">
                        <li class="table-header">
                            <div class="col col-1">Date</div>
                            <div class="col col-2">From</div>
                            <div class="col col-3">To</div>
                        </li>

                        <TableRow date={'2020/2/4'} from={'1.00 EUR'} to={'1.23494 USD'} />
                        <TableRow date={'2020/3/4'} from={'2.00 EUR'} to={'2.334 USD'} />
                        <TableRow date={'2020/4/5'} from={'4.40 EUR'} to={'8.23496 USD'} />
                    </ul>
                </div>

            </div>

        </div>
    );
}

export default History;