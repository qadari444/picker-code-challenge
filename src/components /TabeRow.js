import React from 'react';



function TableRow({date, from, to}) {
    return (
        <li class="table-row">
        <div class="col col-1" data-label="Job Id">{date}</div>
        <div class="col col-2" data-label="Customer Name">{from}</div>
        <div class="col col-3" data-label="Amount">{to}</div>
    </li>
);
}

export default TableRow;