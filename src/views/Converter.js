import React from 'react';
import { Link } from 'react-router-dom'

function Converter() {
    return (
        <div class="converter">
            <div class="converter-showcase">
                <div class="brand text-white">picker</div>
                <h1 class="lorem text-white">Convert currencies in real-time.
            </h1>
                <div class="card col-9">
                    <div class="card-body">
                        <form class="converter-form">
                            <div class="form-row" style={{ 'padding': '0' }}>
                                <div class="form-group col-md-3">
                                    <label for="amount">Amount</label>
                                    <input type="text" class="form-control" id="amount" value="1.00" />
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="from">From</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <input type="text" class="form-control" id="inlineFormInputGroupUsername"
                                                value="EUR" placeholder="Username" />
                                            <div class="input-group-text" style={{ borderRadius: '0 7px 7px 0' }}>
                                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="to">To</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <input type="text" class="form-control" id="inlineFormInputGroupUsername"
                                                value="USD" />
                                            <div class="input-group-text" style={{ borderRadius: '0 7px 7px 0' }}>
                                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group offset-md-1 col-md-2">
                                    <label for="" class="text-white">submit</label>
                                    <button class="form-control bt btn-danger">Convert</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="output-area">
                <Link class="d-block history-link text-black text-right p-5" to='/converter/history'>
                    view conversion history >
            </Link>
                <div class="calculation" style={{ paddingTop: '40px', paddingLeft: '92px' }}>
                    <p>1 EURO =</p>
                    <h1>1.12392 USD</h1>
                </div>
            </div>
        </div>
    );
}

export default Converter;